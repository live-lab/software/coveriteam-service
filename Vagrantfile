# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# -*- mode: ruby -*-
# vi: set ft=ruby :


# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/jammy64"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false # change this to false if you want a headless VM
    vb.cpus = "2" # change this for a different number of vCPUs on the VM
    vb.memory = "5000" # change this to adapt the RAM available to the VM (in MB)
    vb.name = "coveriteam-service-vm" # change this to use a different name for the VM
  end

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  config.vm.network "forwarded_port", guest: 5000, host: 5000, host_ip: "127.0.0.1"

  

  config.vm.provision :shell, :path => "./vagrant_provision_base.sh"
  
  config.vm.provision :reload

  config.vm.provision "repository", type: "shell", privileged: false, inline: <<-SHELL
    sudo rm -rf coveriteam-service
    git clone /vagrant coveriteam-service
    cd coveriteam-service 
    git submodule update --init
  SHELL

  config.vm.provision "service", type:"shell", privileged:true, :path => "./vagrant_provision_service.sh"


end
