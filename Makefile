# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

.PHONY: tests artifact.zip
.DEFAULT_GOAL := tests

benchexec:
	git clone --depth 1 --branch 3.15 https://github.com/sosy-lab/benchexec.git

downloads: benchexec


standalone-artifact.ova: Vagrantfile compact.sh vagrant_provision_base.sh vagrant_provision_service.sh
	vagrant destroy -f
	vagrant up
	vagrant halt
	vboxmanage sharedfolder remove coveriteam-service-vm --name vagrant
	rm -f standalone-artifact.ova
	./compact.sh
	vboxmanage export coveriteam-service-vm -o standalone-artifact.ova
	vagrant destroy -f

artifact.zip: # standalone-artifact.ova
	rm -rf artifact
	mkdir artifact
	cd artifact && git clone ../ coveriteam-service
	cd artifact/coveriteam-service && git submodule update --init
	cp standalone-artifact.ova artifact/ 
	cp doc/Artifact.md artifact/README.md
	cd artifact && zip CVTS-artifact-ICSE23-submission.zip -r *