# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config # allow login via password for convenience
apt-get update
apt-get upgrade
apt-get install -y \
       gcc-multilib \
       libgomp1 \
       make \
       clang \
       clang-11 \
       clang-12 \
       clang-tidy \
       expect \
       llvm \
       llvm-dev \
       g++-multilib \
       lcov \
       libc6-dev-i386 \
       libclang-cpp-dev \
       libclang-dev \
       libgmp-dev \
       libmpfr6 \
       libmpfr-dev \
       libtinfo5 \
       libz3-4 \
       llvm-11 \
       llvm-12 \
       mono-runtime \
       openjdk-8-jdk-headless \
       openjdk-11-jdk-headless \
       openjdk-17-jdk-headless \
       openmpi-bin \
       python3-absl \
       python3-requests \
       python3-flask \
       python3-flask-cors \
       python3-gunicorn \
       python3-astunparse \
       python3-certifi \
       python3-flatbuffers \
       python3-gast \
       python3-h5py \
       python3-idna \
       python3-pandas \
       python3-sklearn \
       python3-lxml \
       python3-mpi4py \
       python3-networkx \
       python3-numpy \
       python3-packaging \
       python3-pycparser \
       python3-requests \
       python3-setuptools \
       python3-termcolor \
       python3-typing-extensions \
       python3-urllib3 \
       python3-wrapt \
       sbcl \
       python2

# Setup CGroups v1
if [ -f /sys/fs/cgroup/cgroup.controllers ]; then \
echo 'Setting cgroup to version 1 ...'; \
sudo echo 'GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=0"' | sudo tee -a /etc/default/grub; \
sudo update-grub; \
fi


# Setup Benchexec
wget --quiet https://github.com/sosy-lab/benchexec/releases/download/3.15/benchexec_3.15-1_all.deb
dpkg -i benchexec_3.15-1_all.deb
rm benchexec_3.15-1_all.deb
adduser vagrant benchexec
