<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

Welcome to **CoVeriTeam Service**!

You can use this service to execute a verification tool remotely.
The user needs to provide the information about the tool they want to execute
 and the verification task, and the service takes care of the rest.

![Abstract view of the system](abstract-view.png)

## There are a few ways to use this service:
- [Using a cURL command](#curl)
- [Using our web page](#webui)
- [Using CoVeriTeam](#remote)

### Additionally, this service has been used for the following:

- Continuous integration scripts of the 2021 competitions on [software verification](https://sv-comp.sosy-lab.org/2023/) and [testing](https://test-comp.sosy-lab.org/2023/).
  The tool submission for these competitions is managed through GitLab repositories.
  This service was used to test the tools on sample inputs to cross check if they can be executed successfully.
  Following are the repositories for:
  - software verification competition: [https://gitlab.com/sosy-lab/sv-comp/archives-2023](https://gitlab.com/sosy-lab/sv-comp/archives-2023)
  - testing competition: [https://gitlab.com/sosy-lab/test-comp/archives-2023](https://gitlab.com/sosy-lab/sv-comp/archives-2023)
- Demonstration of incremental verification as a service: 
  Some verification tools can export auxiliary verification artifacts like invariants, or predicates.
  CoVeriTeam service was used to develop a solution for incremental verification based on this kind of verification tools.
  The solution is in itself a service, which uses CoVeriTeam service.
  We have demonstrated its use in a CI for an example GitLab project: [https://gitlab.com/sosy-lab/research/data/incremental-ci-demo](https://gitlab.com/sosy-lab/research/data/incremental-ci-demo)
 
## Links

- [WebUI](https://coveriteam-service.sosy-lab.org/webui)
- [Incremental Service Demo](https://gitlab.com/sosy-lab/research/data/incremental-ci-demo)
- Incremental Endpoint: [https://coveriteam-service.sosy-lab.org/ivaas/run](https://coveriteam-service.sosy-lab.org/ivaas/run)
- Execution Endpoint: [https://coveriteam-service.sosy-lab.org/execute](https://coveriteam-service.sosy-lab.org/execute)
- [SV-COMP CI](https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/blob/main/.gitlab-ci.yml#L24)
   + [Actual Script](https://gitlab.com/sosy-lab/benchmarking/competition-scripts/-/blob/main/test/check-archives-with-service.py)
- [CoVeriTeam Client](#steps-to-follow-to-test-a-verifier-or-a-tester)

## Execution using cURL{#curl}

You can use cURL or any other REST client to call the service. The service expects a `multipart/form-data` encoded
POST request with the form field `args` containing a json string containing the inputs to CoVeriTeam and all the files
with the names matching how they are referenced in `args`. 

**Example:**

`example_request.json`:
```json
{
    "coveriteam_inputs": {
        "verifier_path": "cpa-seq.yml",
        "program_path": "test02.c",
        "specification_path": "unreach-call.prp",
        "verifier_version": "default",
        "data_model": "ILP32"
    },
    "cvt_program": "verifier.cvt",
    "working_directory": "coveriteam/examples",
}
```
```shell
curl -X POST -H "ContentType: multipart/form-data" -k \
  --form args="$(cat example_request.json)" \
  --form "cpa-seq.yml"=@data/cpa-seq.yml \
  --form "test02.c"=@data/test02.c \
  --form "unreach-call.prp"=@data/unreach-call.prp \
  --form "verifier.cvt"=@data/verifier.cvt \
  --form "verifier_resource.yml"=@data/verifier_resource.yml \
  --output cvt_remote_output.zip \
  coveriteam-service.sosy-lab.org/execute
```

## Execution using web page{#webui}

Users can try out some verification tools using our service
with the help of a simple UI: [https://coveriteam-service.sosy-lab.org/webui](https://coveriteam-service.sosy-lab.org/webui)
The user needs to select the verification tool, the property, and upload a C program,
and then simply press *Go*.
The web page shows the progress, and outputs the result of the verification.
The page supports C verification tools from the [12th competition on software verification](https://sv-comp.sosy-lab.org/2023/).


## Execution using CoVeriTeam{#remote}
CoVeriTeam comes with an integrated client for this service.
Users can simply add `--remote` to a command given to CoVeriTeam
and it would execute the tool remotely on the server.
More information is available in the CoVeriTeam repo: 
[https://gitlab.com/sosy-lab/software/coveriteam/-/blob/main/doc/remote.md](https://gitlab.com/sosy-lab/software/coveriteam/-/blob/main/doc/remote.md)

### FAQs
1. Why don't I just see the output produced by my tool on `stdout`? \
   You can use the option `--verbose` for this. 
   The output you see is produced by `CoVeriTeam` for the executed program (which might execute more than one tools).
   With `-verbose` we print the output logs from the tools.
2. My tool was executed with different options than the ones defined in benchmark definitions. Why?\
   `CoVeriTeam` reads options from the [actor definition files](https://gitlab.com/sosy-lab/software/coveriteam/-/tree/main/actors) that
    were created from benchmark definition files but might be outdated. 
    You can either create a merge request to update the actor definition, 
    or report it in the issue tracker of verifier archives or tester archives.
3. Executing the tool using `CoVeriTeam service` behaves differently than the official pre-runs. Why?\
   We know of one scenario where this could happen.
   The service would transport the complete tool folder to the cloud,
   whereas running it with `benchexec` would transport only the required paths 
   set using the variable `REQUIRED_PATHS` in the tool info module.
   This might result in a successful execution using the service, but not with `benchexec`.
   
