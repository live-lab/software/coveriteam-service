<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

Welcome to **CoVeriTeam Service**!

You can use this service to execute a verification tool remotely.
The user needs to provide the information about the tool they want to execute
 and the verification task, and the service takes care of the rest.
The service web-page is hosted under [https://coveriteam-service.sosy-lab.org](https://coveriteam-service.sosy-lab.org/static/index.html).

![Abstract view of the system](static/abstract-view.png)

There are a few ways to use this service:
- [Using a cURL command](#curl)
- [Using our web page](#webui)
- [Using CoVeriTeam](#remote)

Additionally, this service has been used for the following:
- Continuous integration scripts of the 2021 competitions on [software verification](https://sv-comp.sosy-lab.org/2023/) and [testing](https://test-comp.sosy-lab.org/2023/).
  The tool submission for these competitions is managed through GitLab repositories.
  This service was used to test the tools on sample inputs to cross check if they can be executed successfully.
  Following are the repositories for:
  - software verification competition: https://gitlab.com/sosy-lab/sv-comp/archives-2023
  - testing competition: https://gitlab.com/sosy-lab/test-comp/archives-2023
- Demonstration of incremental verification as a service: 
  Some verification tools can export auxiliary verification artifacts like invariants, or predicates.
  CoVeriTeam service was used to develop a solution for incremental verification based on this kind of verification tools.
  The solution is in itself a service, which uses CoVeriTeam service.
  We have demonstrated its use in a CI for an example GitLab project: https://gitlab.com/sosy-lab/research/data/incremental-ci-demo
 
## Use Cases

- [WebUI](https://coveriteam-service.sosy-lab.org/webui)
- [Incremental Service Demo](https://gitlab.com/sosy-lab/research/data/incremental-ci-demo)
- Incremental Endpoint: https://coveriteam-service.sosy-lab.org/ivaas/run
- [SV-COMP CI](https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/blob/main/.gitlab-ci.yml#L24)
   + [Actual Script](https://gitlab.com/sosy-lab/benchmarking/competition-scripts/-/blob/main/test/check-archives-with-service.py)
- [CoVeriTeam Client](#steps-to-follow-to-test-a-verifier-or-a-tester)

## Execution using cURL

You can use cURL or any other REST client to call the service. The service expects a `multipart/form-data` encoded
POST request with the form field `args` containing a json string containing the inputs to CoVeriTeam and all the files
with the names matching how they are referenced in `args`. 

**Example:**

`example_request.json`:
```json
{
    "coveriteam_inputs": {
        "verifier_path": "../actors/cpa-seq.yml",
        "program_path": "../../sv-benchmarks/c/ldv-regression/test02.c",
        "specification_path": "../../sv-benchmarks/c/properties/unreach-call.prp",
        "verifier_version": "default",
        "data_model": "ILP32"
    },
    "cvt_program": "verifier.cvt",
    "working_directory": "coveriteam/examples",
}
```
```shell
curl -X POST -H "ContentType: multipart/form-data" -k \
    --form args="$(cat example_request.json)" \
    --form "../actors/cpa-seq.yml"=@data/cpa-seq.yml \
    --form "../../sv-benchmarks/c/ldv-regression/test02.c"=@data/test02.c \
    --form "../../sv-benchmarks/c/properties/unreach-call.prp"=@data/unreach-call.prp \
    --form "verifier.cvt"=@data/verifier.cvt \
    --form "../actors/verifier_resource.yml"=@data/verifier_resource.yml \
    --output cvt_remote_output.zip \
    <SERVICE_URL>
```

## Execution using web page

Users can try out some verification tools using our service
with the help of a simple UI: https://coveriteam-service.sosy-lab.org/webui
The user needs to select the verification tool, the property, and upload a C program,
and then simply press *Go*.
The web page shows the progress, and outputs the result of the verification.
The page supports C verification tools from the [12th competition on software verification](https://sv-comp.sosy-lab.org/2023/).


## Execution using CoVeriTeam
CoVeriTeam comes with an integrated client for this service.
Users can simply add `--remote` to a command given to CoVeriTeam
and it would execute the tool remotely on the server.
More information is available in the CoVeriTeam repo: 
https://gitlab.com/sosy-lab/software/coveriteam/-/blob/main/doc/remote.md

## Setup your own instance

You can use the provided `start.sh` script to launch a container running CoVeriTeam Service.
The launched container exposes CoVeriTeam Service to `127.0.0.1:5000`. 
To run a VM please refer to [doc/Artifact-Creation.md](doc/Artifact-Creation.md).

The `start.sh` script has several parameters:
```
start.sh
    [-c|--container] launch CoVeriTeam Service in a Container 
    [-b|--build] build the Container Image (necessary before first launch)
    [-d|--dev] start a development server instead of gunicorn
    [--cache <CACHEDIR>] use <CACHEDIR> to as cache for CoVeriTeam
    [-e|--extra-arg]* specify extra arguments to be passed to the container run command 
    [--use-docker] use docker instead of podman for all container commands
```


### FAQs
1. Why don't I just see the output produced by my tool on `stdout`? \
   You can use the option `--verbose` for this. 
   The output you see is produced by `CoVeriTeam` for the executed program (which might execute more than one tools).
   With `-verbose` we print the output logs from the tools.
2. My tool was executed with different options than the ones defined in benchmark definitions. Why?\
   `CoVeriTeam` reads options from the [actor definition files](https://gitlab.com/sosy-lab/software/coveriteam/-/tree/main/actors) that
    were created from benchmark definition files but might be outdated. 
    You can either create a merge request to update the actor definition, 
    or report it in the issue tracker of verifier archives or tester archives.
3. Executing the tool using `CoVeriTeam service` behaves differently than the official pre-runs. Why?\
   We know of one scenario where this could happen.
   The service would transport the complete tool folder to the cloud,
   whereas running it with `benchexec` would transport only the required paths 
   set using the variable `REQUIRED_PATHS` in the tool info module.
   This might result in a successful execution using the service, but not with `benchexec`.
   
