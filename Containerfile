# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

FROM registry.gitlab.com/sosy-lab/benchmarking/competition-scripts/user:2023

WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt install python3-pip
# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY ./server /usr/src/app/server
COPY ./static /usr/src/app/static
COPY ./wsgi.py /usr/src/app/wsgi.py
COPY ./wsgi.py /usr/src/app/server.wsgi
COPY ./coveriteam /usr/src/app/coveriteam

RUN apt update
RUN apt install git wget gcc build-essential -y
RUN git clone https://github.com/sosy-lab/benchexec.git /usr/src/benchexec
RUN cd ../benchexec && pip install .
RUN ln -s /usr/src/benchexec /usr/src/app/coveriteam/benchexec


ENTRYPOINT [ "gunicorn", "--bind", "0.0.0.0:5000", "wsgi:app" ]