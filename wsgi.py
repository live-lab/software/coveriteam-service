# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from server.service import app
import sys

sys.dont_write_bytecode = True  # prevents writing .pyc files

if __name__ == "__main__":
    app.run()
