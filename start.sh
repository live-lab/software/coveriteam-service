#!/bin/bash
# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

RUNTIME="/usr/bin/crun"
ENTRYPOINT="" #the entry point is empty in prod setting
CACHE_DIR="${PWD}/.cache"
CACHE_OPT=0
CONTAINER=0
DEV=0
EXTRA=""
DOBUILD=0
RUNCMD="podman --runtime ${RUNTIME}"
ANNOTATIONS=('--annotation' 'run.oci.keep_original_groups=1' '--security-opt' 'unmask=/proc/*' '--security-opt' 'seccomp=unconfined')

while [[ $# -gt 0 ]]; do
  case $1 in
    -c|--container)
      CONTAINER=1
      shift # past argument
      ;;
    -b|--build)
      DOBUILD=1
      shift # past argument
      ;;
    -d|--dev)
      DEV=1
      ENTRYPOINT="--entrypoint python -m server.service"
      shift # past argument
      ;;
    --cache)
      CACHE_OPT=1
      CACHE_DIR="$2"
      shift # past argument
      shift # past value
      ;;
    -e|--extra-arg)
      EXTRA+="$2"
      shift # past argument
      shift # past value
      ;;
    --use-docker)
      shift
      RUNCMD="sudo docker"
      ANNOTATIONS=('--privileged')
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

if [[ $CACHE_OPT == 1 && $CONTAINER == 0 ]]; then
    echo "--cache option is ignored if cvt service is not ran as container"
fi

if [ $DOBUILD == 1 ]; then
    echo "Building Image..."
    $RUNCMD build . -t cvt-service -f Containerfile
fi

if [ $CONTAINER == 1 ]; then
    echo "Using provider: ${RUNCMD}"
    echo "with annotations: ${ANNOTATIONS[@]}"
    mapped_cache=$(toml get --toml-path server/config.toml config.cache)
    if [ ! "$mapped_cache" == CACHE_DIR ]; then
        echo "You can change the location insight the container in server/config.toml config.cache ."
        echo "You can change the location of the cache that is mapped into the container with --cache path/to/dir"
    fi
    echo $inform_cache
    if [[ ! "$mapped_cache" = /* ]]; then # is relative
        mapped_cache="/usr/src/app/$mapped_cache"
    fi
    echo "Mapping local cache dir $CACHE_DIR to $mapped_cache inside container"
    $RUNCMD \
    run $ENTRYPOINT \
    "${ANNOTATIONS[@]}" \
    --rm \
    -p 5000:5000 \
    -v /sys/fs/cgroup:/sys/fs/cgroup \
    -v $PWD/server/config.toml:/usr/src/app/server/config.toml \
    -v $CACHE_DIR:$mapped_cache:rw \
    --name cvt_service \
    $EXTRA \
    cvt-service:latest
elif [ $DEV == 1 ]; then 
    if [[ $(toml get --toml-path server/config.toml policies.benchexec.debug) == "False" ]]; then 
        echo "To enable debug mode set policies.benchexec.debug=true in server/config.toml"
    fi
    python -m server.service
else 
    gunicorn --bind 127.0.0.1:5000 wsgi:app
fi
