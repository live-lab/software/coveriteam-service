#!/bin/bash

# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -xe

VMNAME="coveriteam-service-vm"

VMDKPATH=$(vboxmanage showvminfo $VMNAME  --machinereadable |grep SCSI-0-0 | cut -d "=" -f 2 | tr -d '"')
VDIPATH=$(dirname "${VMDKPATH}")/$(basename "${VMDKPATH}").vdi

echo "Mounting existing disk image and applying zerofree...."
mkdir -p mnt
qemu-nbd --socket $PWD/socket.sock --cache=unsafe --discard=unmap "${VMDKPATH}" &
nbdfuse mnt/nbd1 --command nbdkit -s nbd socket=socket.sock --filter=partition partition=1 &
sleep 3
zerofree -v mnt/nbd1
fusermount -u ./mnt
echo "done"

echo "Cloning disk"
CLONEHD=$(vboxmanage showvminfo $VMNAME  --machinereadable |grep SCSI-0-0 | cut -d "=" -f 2 | tr -d '"')
vboxmanage clonehd "${CLONEHD}" "${VDIPATH}" --format vdi
echo "Attaching new vdi disk instead of the vmdk disk"
vboxmanage storageattach $VMNAME --storagectl "SCSI" --device 0 --port 0 --type hdd --medium "${VDIPATH}"
echo "Deleting old vmdk disk"
vboxmanage closemedium "${VMDKPATH}" --delete
