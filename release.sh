#!/bin/bash

#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Guide to use this script:
# 1. version as a parameter. It should be new, i.e, different than the one in the init file.
#    This is automatically updated in the init file.
# 2. Update the change log with the contents of this version.
# 3. There should be no local changes. Either commit or stash them.

set -e

if [ -z "$1" ]; then
  echo "Please specify to-be-released version as parameter."
  exit 1
fi

OLD_VERSION="$(grep __version__ server/__init__.py | sed -e 's/^.*"\(.*\)".*$/\1/')"
VERSION="$1"
if [ $(expr match "$VERSION" ".*dev") -gt 0 ]; then
  echo "Cannot release development version."
  exit 1
fi
if [ "$VERSION" = "$OLD_VERSION" ]; then
  echo "Version already exists."
  exit 1
fi
if [ ! -z "$(git status -uno -s)" ]; then
  echo "Cannot release with local changes, please stash them."
  exit 1
fi

# Prepare files with new version number
sed -e "s/^__version__ = .*/__version__ = \"$VERSION\"/" -i server/__init__.py
git commit server/__init__.py -m "Release $VERSION"

# Upload and finish
read -p "Everything finished, do you want to release version '$VERSION' publically? (y/n) " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]; then
  exit 0
fi

git push --tags

read -p "Please enter next version number:  " -r
sed -e "s/^__version__ = .*/__version__ = \"$REPLY\"/" -i server/__init__.py
git commit server/__init__.py -m"Prepare version number for next development cycle."


echo
