# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from copy import deepcopy
from pathlib import Path
import unittest
import json
import io
from unittest import mock
from server.contracts import InputDataContract
from server.service import app

from server.vcloud_executor import RequestProcessorVCloud

app.config["TESTING"] = True

# Think multiple times before changing this request template
fnames = [
    "../actors/cpa-seq.yml",
    "../actors/verifier_resource.yml",
    "../../sv-benchmarks/c/ldv-regression/test02.c",
    "../../sv-benchmarks/c/properties/unreach-call.prp",
    "verifier.cvt",
]
cvt_inputs = {
    "verifier_path": "../actors/cpa-seq.yml",
    "program_path": "../../sv-benchmarks/c/ldv-regression/test02.c",
    "specification_path": "../../sv-benchmarks/c/properties/unreach-call.prp",
}
REQUEST_TEMPLATE = {
    "working_directory": "examples",
    "coveriteam_inputs": cvt_inputs,
    "cvt_program": "verifier.cvt",
}

RESOURCES = Path(__file__).parent / "Resources"


def check_message(response, msg):
    returned_message = response.json.get("message")
    assert msg in returned_message


def dummy_files(filenames):
    return {fname: (io.BytesIO(b"some content"), fname) for fname in filenames}


class TestService(unittest.TestCase):
    def setUp(self):
        self.req = deepcopy(REQUEST_TEMPLATE)

    def test_missing_cvt_program(self):
        with app.test_client() as c:
            # removing the last element: cvt file
            files = fnames[:-1]
            response = c.post(
                "/execute",
                data={
                    "args": json.dumps(self.req),
                    **dummy_files(files),
                },
            )

            assert response.status_code == 400
            check_message(
                response,
                "CoVeriTeam Service did not receive a CoVeriTeam program to execute",
            )

    @unittest.skip("To be included again in a future version")
    def test_missing_filename(self):
        with app.test_client() as c:
            # removing the first element: yaml file
            files = fnames[1:]
            response = c.post(
                "/execute", data={"args": json.dumps(self.req), **dummy_files(files)}
            )
            assert response.status_code == 400
            check_message(response, "passed as an input to CoVeriTeam but not uploaded")

    @unittest.skip("To be included again in a future version")
    def test_filename_notallowed1(self):
        with app.test_client() as c:
            fnames1 = fnames + ["\x00"]
            files = fnames1
            response = c.post(
                "/execute", data={"args": json.dumps(self.req), **dummy_files(files)}
            )
            assert response.status_code == 400
            check_message(response, "The filename extension of \x00 is not allowed")

    def test_workding_dir_absolute(self):
        with app.test_client() as c:
            self.req["__working_dir"] = "\\etc\\abc"
            response = c.post(
                "/execute", data={"args": json.dumps(self.req), **dummy_files(fnames)}
            )
            assert response.status_code == 400
            check_message(response, "Found some unexpected input in the json:")

    def test_more_inputs_than_specified(self):
        with app.test_client() as c:
            self.req["extra_input"] = "extra"
            response = c.post(
                "/execute", data={"args": json.dumps(self.req), **dummy_files(fnames)}
            )
            assert response.status_code == 400
            check_message(response, "Found some unexpected input in the json:")

    def test_inputs_not_dict(self):
        with app.test_client() as c:
            response = c.post("/execute", data={"args": "abc", **dummy_files(fnames)})
            assert response.status_code == 400

    @unittest.skip("Debug helper")
    def test_input_json_to_contract(self):
        with (RESOURCES / "test_input.json").open("r") as fd:
            payload = json.load(fd)

        ip = InputDataContract(payload)
        rp = RequestProcessorVCloud(ip)
        rp._create_required_files()

        print(rp.version_map)  # noqa: T201

    TEST_DEBUG = False

    @unittest.skipIf(condition=(not TEST_DEBUG), reason="Debug helper")
    def test_input_json(self):

        with app.test_client() as c, (RESOURCES / "no_filenames.json").open("r") as fd:
            with mock.patch("tempfile.mkdtemp") as mock_mkdtemp:
                mock_mkdtemp.return_value = "testtmp"
                data = fd.read()
                cpa_seq = RESOURCES / "cpa-seq.yml"
                verifier = RESOURCES / "verifier.cvt"
                test02 = RESOURCES / "test02.c"
                verifier_resource = RESOURCES / "verifier_resource.yml"
                unreach_call = RESOURCES / "unreach-call.prp"
                response = c.post(
                    "/v2/execute",
                    data={
                        "args": data,
                        "../../sv-benchmarks/c/ldv-regression/test02.c": test02.open(
                            "rb"
                        ),
                        "verifier.cvt": verifier.open("rb"),
                        "../actors/cpa-seq.yml": cpa_seq.open("rb"),
                        "../../sv-benchmarks/c/properties/unreach-call.prp": (
                            unreach_call.open("rb")
                        ),
                        "../actors/verifier_resource.yml": verifier_resource.open("rb"),
                    },
                )
                assert response.status_code == 200

    @unittest.skip("nottest")
    def test_working_dir_accessing_parent1(self):
        # This test don't make sense as we are sanitizing the inputs.
        with app.test_client() as c:
            self.req["working_directory"] = "../examples"
            response = c.post("/execute", data={"args": json.dumps(self.req)})
            assert response.status_code == 400
            check_message(response, "")

    @unittest.skip("nottest")
    def test_working_dir_accessing_parent2(self):
        # This test don't make sense as we are sanitizing the inputs.
        with app.test_client() as c:
            self.req["working_directory"] = "examples/../abc"
            response = c.post("/execute", data={"args": json.dumps(self.req)})
            assert response.status_code == 400
            check_message(response, "")
