# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import re
from pathlib import Path
from abc import ABC, abstractmethod
from typing import Dict, List, Optional, Union, cast
from server import InputValidationException
from pprint import PrettyPrinter
from werkzeug.datastructures import FileStorage


def check_str_or_list(s):
    if isinstance(s, str) and s:
        return s
    if not isinstance(s, list) and s:
        raise InputValidationException(
            "Received an input which is not a string or a list of strings."
        )
    for x in s:
        check_string(x)
    return s


def type_check_input_data_contract(input_dc):
    check_string(input_dc.working_directory)
    check_string(input_dc.cvt_program)
    for key, value in input_dc.coveriteam_inputs.items():
        check_string(key)

        check_str_or_list(value)

    for f in input_dc.files.keys():
        check_string(f)


class AbstractInputSanitizer(ABC):
    def __init__(self, input_data_contract):
        self.input = input_data_contract

    def sanitize_input(self):
        self.sanitize_working_directory()
        self.sanitize_cvt_program()
        self.sanitize_coveriteam_inputs()
        self.sanitize_files()

    @abstractmethod
    def sanitize_working_directory(self):
        pass

    @abstractmethod
    def sanitize_cvt_program(self):
        pass

    @abstractmethod
    def sanitize_coveriteam_inputs(self):
        pass

    @abstractmethod
    def sanitize_files(self):
        pass


def check_string(s):
    if not isinstance(s, str) and s:
        raise InputValidationException("Received an input which is not a string.")
    return s


class InputDataContract:
    """
    The request processor of the service will act on objects of this class.
    This class has following fields:
        - "working_directory":
            working directory from which the user is executing CoVeriTeam
        - "coveriteam_inputs":
            inputs provided to the CoVeriTeam program by the user in form of
            a list of tuples containing parameter name and its value (a file path)
        - "cvt_program": CoVeriTeam program to execute
        - "files": dict of filename and file object.
        - "additional_parameters": an optional field.
            It is a dict containing parameters passed to CoVeriteam.
            ATM we support only data-model, but later maybe we add others.
    """

    def __init__(self, args, files=None):
        if not isinstance(args, dict):
            raise InputValidationException("Did not receive an expected input.")
        if files and not isinstance(files, dict):
            raise InputValidationException("Did not receive an expected input.")
        # Process optional fields first.
        self.additional_parameters = args.pop("additional_parameters", None)
        try:
            self.working_directory: str = args.pop("working_directory")
            self.coveriteam_inputs: Dict[  # noqa TAE002
                str, Union[str, List[str]]
            ] = args.pop("coveriteam_inputs")
            self.cvt_program: str = args.pop("cvt_program")

            if files:
                self.files: Dict[str, Optional[FileStorage]] = cast(
                    Dict[str, FileStorage], files
                )
                args.pop("filenames", None)
            else:
                filenames = args.pop("filenames")
                self.files: Dict[str, Optional[FileStorage]] = dict.fromkeys(filenames)

            if self.cvt_program not in self.files.keys():
                raise InputValidationException(
                    "CoVeriTeam Service did not receive a CoVeriTeam program to execute"
                )
            # By this time all the keys from the dict should have been exhausted.
            if args:
                raise InputValidationException(
                    "Found some unexpected input in the json: %s" % (args.keys())
                )

        except KeyError:
            raise InputValidationException(
                "Provided input does not contain all the required fields."
            )

        if not isinstance(self.coveriteam_inputs, dict):
            raise InputValidationException(
                "Malformed inputs. Inputs must be a dictionary, current type is %s",
                type(self.coveriteam_inputs),
            )
        type_check_input_data_contract(self)

    def __repr__(self):
        pp = PrettyPrinter(indent=2, width=80, sort_dicts=True)
        return f"""
    InputDataContract
{
        pp.pformat(dict(
        working_directory=self.working_directory,
        coveriteam_inputs=self.coveriteam_inputs,
        cvt_program=self.cvt_program,
        files=self.files)
        )}
    """.lstrip()


class InputValidatorBase:
    """
    This is the base class for input validation. It provides the mandatory validation.
    Additional validation should be added to the inherited class overriding the function
    custom_validation.
    Additionally, the inheriting classes should also override the function
    check_allowed_file_path to add checks to the file paths.

    The inherited classes MUST NOT:
        1) override validate_input.
        2) define its own __init__
        3) override methods beginning with __validate
    """

    ALLOWED_FILE_EXTENSIONS = {"cvt", "c", "i", "java", "prp", "yml", "txt"}

    NOT_A_FILE_INPUTS = frozenset({"version", "data_model"})

    def __init__(self, input_data_contract):
        self.input = input_data_contract
        type_check_input_data_contract(self.input)

    def validate_input(self):
        # TODO add a final attribute to this function.
        self.__validate_working_directory()
        self.__validate_cvt_program()
        self.__validate_coveriteam_inputs()
        self.__validate_files()
        self.custom_validation()

    def __validate_working_directory(self):
        pass

    def __validate_cvt_program(self):
        if self.input.cvt_program not in self.input.files.keys():
            raise InputValidationException(
                "I did not find the CoVeriTeam program to execute."
            )

    def __validate_coveriteam_inputs(self):
        for param, val in self.input.coveriteam_inputs.items():
            # Check for the names of parameters to be passed to CoVeriTeam
            # only aplanumeric allowed in argument names.
            if not re.match(r"\w+$", param):
                raise InputValidationException(
                    "This parameter name is not allowed: %s" % param
                )
            # When the current input doesn't require a file skip the file check
            if any(
                (not_a_file_input in param)
                for not_a_file_input in self.NOT_A_FILE_INPUTS
            ):
                continue

            # we simply check that these strings should all be included in filenames.
            # check for exact matching is not enough because, for example,
            # java verifiers take a directory, not a file as an input.
            if not any(f.startswith(val) for f in self.input.files.keys()):
                raise InputValidationException(
                    "The path %s is passed as an input to CoVeriTeam but not uploaded."
                    % val,
                )

    def __validate_files(self):
        for path in self.input.files.keys():
            fp = Path(path)
            # 1. check file extension and fancy names
            if fp.suffix[1:] not in self.ALLOWED_FILE_EXTENSIONS:
                raise InputValidationException(
                    "The filename extension of %s is not allowed." % fp
                )
            # 2. No absolute path
            if fp.is_absolute():
                raise InputValidationException(
                    "Absolute paths are not allowed. Absolute path found: %s" % fp
                )
            self.check_allowed_file_path(fp)

    def check_allowed_file_path(self, path):
        # This should be defined by the inheriting class to further filter the paths.
        pass

    def custom_validation(self):
        # This should be defined by the inheriting class to add custom validation.
        pass
