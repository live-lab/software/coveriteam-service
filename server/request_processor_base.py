# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import shutil
from pathlib import Path
import logging
import tempfile
from abc import ABC, abstractmethod
from typing import Union
from .contracts import InputDataContract


class AbstractRequestProcessor(ABC):
    def __init__(self, input_data_contract: InputDataContract):
        self._prechecks()
        self.input = input_data_contract
        self._tmp = tempfile.mkdtemp(prefix="cvtremote_")
        logging.debug("created temp directory: %s", self._tmp)
        self._wd = Path(self._tmp) / self.input.working_directory

    def cleanup(self):
        shutil.rmtree(self._tmp, ignore_errors=True)

    def join_wd(self, p: Union[Path, str]) -> Path:
        return self._wd / p

    def process_request(self):
        Path(self._wd).mkdir(parents=True, exist_ok=True)
        self._create_required_files()
        self._execute_coveriteam()
        return self._prepare_response()

    @abstractmethod
    def _prechecks(self):
        pass

    @abstractmethod
    def _create_required_files(self):
        pass

    @abstractmethod
    def _execute_coveriteam(self):
        pass

    @abstractmethod
    def _prepare_response(self):
        pass
