# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from werkzeug.datastructures import FileStorage
import json
import logging
import uuid
from flask import request
from datetime import datetime
from . import config
import os


def setup():
    if config.EXTENDED_LOGGING:
        os.makedirs(config.EXTENDED_LOG_DIR, exist_ok=True)


def _read_size(file: FileStorage) -> int:
    size = file.stream.seek(0, os.SEEK_END)
    file.stream.seek(0)  # seek back to the beginning
    return size


def log_request():

    if not config.EXTENDED_LOGGING:
        return

    ts = datetime.now()

    log = {
        "remote_addr": request.remote_addr,
        "files": {key: _read_size(fl) for key, fl in request.files.items()},
        "args": request.form.get("args", None),
        "time": ts.isoformat(),
    }

    filename = ts.strftime("%Y-%m-%d_%H:%M:%S") + str(uuid.uuid4()) + ".json"
    try:
        with (config.EXTENDED_LOG_DIR / filename).open("w+") as fd:
            json.dump(log, fd)
            fd.flush()
    except Exception as e:
        logging.error("Error while logging request: %s", e)  # noqa: G200
