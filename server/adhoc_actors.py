# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass
import io
from pathlib import Path
from typing import IO, List, Optional, Union, overload
import defusedxml.ElementTree as eT
import yaml

import requests

BENCHDEF_TEMPLATE = (
    "https://gitlab.com/sosy-lab/sv-comp/bench-defs/-"
    "/raw/main/benchmark-defs/{tool}.xml"
)

StrPath = Union[str, Path]
FileRead = Union[io.BytesIO, StrPath]


@dataclass
class ActorDef:
    name: str
    tool_info: str
    options: List[str]


def download_benchdef_xml(tool: str) -> io.BytesIO:
    link = BENCHDEF_TEMPLATE.format(tool=tool)
    resp = requests.get(link)
    return io.BytesIO(resp.content)


def parse_benchdef_options(benchdef: FileRead, tool_name: str) -> ActorDef:

    root = eT.parse(benchdef).getroot()

    options: List[str] = []
    for option in root.findall("option"):
        name = option.get("name")
        if name is None:
            raise RuntimeError(f"'name' tag missing for an option in xml {benchdef}")
        options.append(name)
        if option.text:
            options.append(option.text)

    tool_info = root.get("tool")
    if tool_info is None:
        raise RuntimeError(f"'tool' tag missing for benchmark xml {benchdef}")

    return ActorDef(tool_name, tool_info, options)


@overload
def assemble_actor_yml(definition: ActorDef, to_file: None) -> None:
    ...


@overload
def assemble_actor_yml(definition: ActorDef, to_file: IO[bytes] = ...) -> "yaml._Yaml":
    ...


def assemble_actor_yml(definition: ActorDef, to_file: Optional[IO[bytes]] = None):
    # Remote location of the tool
    loc = (
        "https://gitlab.com/sosy-lab/sv-comp/archives-2023/"
        f"-/raw/main/2023/{definition.name}.zip"
    )

    raw = {
        "resourcelimits": {
            "memlimit": "8 GB",
            "timelimit": "2 min",
            "cpuCores": "2",
        },
        "actor_name": definition.name,
        "toolinfo_module": definition.tool_info,
        "archives": [
            {"version": "svcomp23", "location": loc, "options": definition.options}
        ],
        "format_version": "1.2",
    }

    if to_file is not None:
        yaml.dump(raw, to_file)
        return None

    return yaml.dump(raw)


def get_actor_for(tool: str):
    benchdef = download_benchdef_xml(tool=tool)

    actor_def = parse_benchdef_options(benchdef, tool)

    return assemble_actor_yml(actor_def)
