#!/usr/bin/env python3

# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import sys
from pathlib import Path

sys.dont_write_bytecode = True  # prevents writing .pyc files

script = Path(__file__).resolve()
sys.path.insert(0, str(script.parent))
from ivaas.server import app as application  # noqa F401
