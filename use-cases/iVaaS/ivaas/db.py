# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
from sqlalchemy import Column, String, LargeBinary
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import create_engine
from fastapi import UploadFile
from .util import digest_file

DB_LOC = "precisiondb.sqlite"

engine = create_engine(f"sqlite:///{DB_LOC}", echo=True)
Base = declarative_base()
Session = sessionmaker(bind=engine)


class IncrementInfo(Base):
    __tablename__ = "increments"

    digest = Column(String(2 * 64), unique=True, primary_key=True)
    increment = Column(LargeBinary())

    def __repr__(self):
        return "<Increment(id='%s', digest='%s', increment='%sb')>" % (
            self.id,
            self.digest,
            self.increment.__sizeof__(),
        )


Base.metadata.create_all(engine)


def _create_test_data():
    _put_precision("deadbeef" * 16, b"true")


def _get_precision(digest: str) -> (bytes | None):

    with Session.begin() as sess:
        query = sess.query(IncrementInfo.increment).filter(
            IncrementInfo.digest == digest
        )

        row = query.one_or_none()
        if row is None:
            return None

        return row[0]


def _put_precision(digest: str, precision: bytes):

    with Session.begin() as sess:
        sess.merge(IncrementInfo(digest=digest, increment=precision))


async def get_precision(program: UploadFile, spec: UploadFile) -> (bytes | None):

    [first, second] = await asyncio.gather(digest_file(program), digest_file(spec))

    digest = first + second
    return _get_precision(digest)


async def put_precision(program: UploadFile, spec: UploadFile, precision: bytes):

    [first, second] = await asyncio.gather(digest_file(program), digest_file(spec))

    digest = first + second
    _put_precision(digest, precision)


_create_test_data()
