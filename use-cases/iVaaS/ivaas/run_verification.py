# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from io import BytesIO
import requests
from fastapi import UploadFile
from pathlib import Path
import asyncio
import json
from .exceptions import VerifierException
from zipfile import ZipFile
import logging

logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")

URL = "https://coveriteam-service.sosy-lab.org/execute"
SCRIPT = Path(__file__).parent
DATA = SCRIPT / "data"


async def prepare_files(
    program: UploadFile,
    spec: UploadFile,
    precision: (bytes | None) = None,
):
    if precision is None:
        precision = b""

    files = {
        "incremental.cvt": (DATA / "incremental.cvt").open("rb"),
        "cpachecker-incr.yml": (DATA / "cpachecker-incr.yml").open("rb"),
        "predmap.txt": ("predmap.txt", precision),
    }

    await asyncio.gather(program.seek(0), spec.seek(0))

    files[spec.filename] = BytesIO(await spec.read())
    files[program.filename] = BytesIO(await program.read())

    return files


def prepare_args(prog_name: str, spec_name: str):

    return {
        "coveriteam_inputs": {
            "program_path": prog_name,
            "specification_path": spec_name,
            "verifier_version": "incr",
            "data_model": "ILP32",
            "predicates_path": "predmap.txt",
        },
        "cvt_program": "incremental.cvt",
        "working_directory": "incremental",
    }


def post(url: str, files: "requests._Files", data: "requests._Data"):
    return asyncio.to_thread(requests.post, url=url, data=data, files=files)


async def call_service(
    program: UploadFile, spec: UploadFile, precision: (bytes | None) = None
):

    files = await prepare_files(program, spec)
    args = prepare_args(program.filename, spec.filename)

    jargs = json.dumps(args)
    return await post(url=URL, data={"args": jargs}, files=files)


async def unpack_precision(raw: bytes) -> bytes:

    fd = BytesIO(raw)

    with ZipFile(fd, "r") as zipf:
        infos = [i for i in zipf.filelist if i.filename.endswith("predmap.txt")]
        if not infos:
            logging.info("No predicate map returned.")
            return b""
        # There should never be more than one;
        # if this is the case regardless take the first
        info = infos[0]
        logging.info("Found predicate map: %s", info.filename)
        with zipf.open(info) as precision:
            return precision.read()


async def unpack_log(raw: bytes) -> str:
    fd = BytesIO(raw)
    with ZipFile(fd, "r") as zipf, zipf.open("LOG") as log:
        return log.read().decode(errors="ignore")


async def process_result(response: requests.Response):

    if response.status_code != 200:
        try:
            msg = response.json()["message"]
            raise VerifierException(msg)
        except json.JSONDecodeError:
            raise VerifierException(
                "An error occurred while calling CoVeriTeam Service"
            )

    [precision, log] = await asyncio.gather(
        unpack_precision(response.content), unpack_log(response.content)
    )

    return precision, log, response.content


async def run_verification(
    program: UploadFile, specification: UploadFile, precision: (bytes | None)
):

    response = await call_service(program, specification, precision)
    return await process_result(response)
