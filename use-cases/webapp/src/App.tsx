// SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

import React, { Dispatch, SetStateAction, SyntheticEvent } from 'react';
import './App.css';
import Button from '@mui/material/Button';
import DownloadIcon from '@mui/icons-material/Download';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import SendIcon from '@mui/icons-material/Send';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import UploadIcon from '@mui/icons-material/Upload';
import LinearProgress from '@mui/material/LinearProgress';
import JSZip from "jszip";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Box from '@mui/material/Box';

import { saveAs } from 'file-saver';

import { Autocomplete, Typography } from '@mui/material';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { verifierC } from './data';

interface CvtResponse {
  log: string
  zip: Blob | null
}

const cvt_service = axios.create({ "baseURL": "https://coveriteam-service.sosy-lab.org" })

async function handleResponse(resp: AxiosResponse<any, any>): Promise<CvtResponse> {
  if (resp.status !== 200) {
    console.error("Status from CoVeriTeam Service was not 200!", resp)
    try {
      const text = JSON.parse(await resp.data.text())
      console.log(text)
      return { "log": text.message, "zip": null }

    } catch (error) {
      console.error(error)
      return { "log": "An error occured: " + resp.data.text(), "zip": null }
    }
  }

  console.log(resp)

  const zip = new JSZip()

  await zip.loadAsync(new Blob([resp.data]))

  let log = await zip.file("LOG")?.async("string")
  console.log(log)

  if (log === undefined) {
    log = ""
  }

  return { log, "zip": new Blob([resp.data]) }

}

function App() {
  const [tool, setTool] = React.useState('');
  const [spec, setSpec] = React.useState('');
  const [display, setDisplay] = React.useState('Nothing yet...');
  const [loading, setLoading] = React.useState(false);


  const [program, setProgram] = React.useState<File | null>(null);

  const [dlContent, setDlContent] = React.useState<Blob | null>(null);

  const handleProgramSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files != null) {
      setProgram(event.target.files[0]);
    }
  }

  const handleSubmit = async () => {
    try {

      setLoading(true)
      let text = "Getting Actor Definition from CoVeriTeam Service..."
      setDisplay(text)

      let actorResponse
      try {
        actorResponse = await cvt_service.get("actordef/" + tool);
      }
      catch (error: any) {
        setDisplay("Error: Please select a tool!")
        setLoading(false)
        return
      }
      const actorContentType = actorResponse.headers['content-type'];
      const actor = new Blob([actorResponse.data], { type: actorContentType });
      text += "\n Done!\n" + await actor.text()
      setDisplay(text)
      const cvtProgram = new Blob([verifierC], { type: "text/plain" });

      // store the states in the form data
      const serviceData = new FormData();
      if (program != null) {
        serviceData.append("args", JSON.stringify(
          {
            coveriteam_inputs: {
              verifier_path: tool + ".yml",
              program_path: program.name,
              specification_path: "spec.prp",
              verifier_version: "svcomp23",
              data_model: "ILP32"
            },
            cvt_program: "verifier-C.cvt",
            working_directory: "coveriteam"
          }
        ));
        // give file name as 3rd argument when appending a file
        serviceData.append('verifier-C.cvt', cvtProgram, 'verifier-C.cvt');
        serviceData.append(tool + '.yml', actor, tool + '.yml');
        serviceData.append(program.name, program, program.name);
        serviceData.append("spec.prp", new Blob([spec]), "spec.prp");
      }

      text += "\nCalling CoVeriTeam Service..."
      setDisplay(text)
      let response
      try {
        // make axios post request
        response = await cvt_service.post("/execute", serviceData, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          responseType: "blob"
        });
      }
      catch (error: any) {
        response = error.response
      }

      const { log, zip } = await handleResponse(response)
      setLoading(false)
      setDlContent(zip)
      text += "Done! Output:\n" + log
      setDisplay(text)

      // TODO get results and zip
    } catch (error) {
      console.log(error);
    }
  }

  const handleDl = () => {
    if (dlContent !== null) {
      saveAs(dlContent, "output.zip")
    }
  }

  return (
    <div className='App'>
      <Stack spacing={1.5} sx={{ width: 400 }}>
        <h1>Test the SV-COMP Verifiers</h1>
        <ToolSelect storeInput={setTool} />
        <Button variant="contained" component="label" endIcon={<UploadIcon />}>
          Upload Program
          <input hidden type="file" accept=".c,.i" onChange={handleProgramSelect} />
        </Button>
        <p>{program?.name}</p>
        <SpecSelect storeInput={setSpec} />
        <Button variant="contained" endIcon={<SendIcon />} onClick={handleSubmit}>Go!</Button>
        {loading && (
          <LinearProgress />
        )}
        <Card>
          <CardContent >
            <Typography sx={{ fontFamily: 'Monospace' }} justifyContent="left" textAlign={'left'} overflow="auto" maxHeight={"600px"} whiteSpace="pre">
              {display}
            </Typography>
          </CardContent>
        </Card>
        <Button variant="contained" onClick={handleDl} disabled={dlContent === null} startIcon={<DownloadIcon />}>Download Zip</Button> {/* TODO offer zip download */}
      </Stack>
    </div >
  );
}

const ToolSelect = ({ storeInput: setTool }: SelectProps) => {
  const handleToolSelect = (_event: SyntheticEvent, newTool: ToolType | null) => {
    if (newTool != null) {
      setTool(newTool.id);
    }
  };

  return (
    <Autocomplete
      id="tool-select"
      options={tools}
      onChange={handleToolSelect}
      autoHighlight
      getOptionLabel={(option) => option.label}
      renderInput={(params) => <TextField {...params} label="Tool" />}
    />
  );
}

const SpecSelect = ({ storeInput: setSpec }: SelectProps) => {
  const handleSpecChange = (event: SelectChangeEvent) => {
    setSpec(event.target.value as string);
  };

  return (
    <FormControl>
      <InputLabel id="spec-select-label">Specification</InputLabel>
      <Select
        labelId="spec-select-label"
        id="spec-select"
        defaultValue=''
        label="Specification"
        onChange={handleSpecChange}
      >
        <MenuItem value={"CHECK( init(main()), LTL(G ! call(reach_error())) )"}>Unreach Call</MenuItem>
        <MenuItem value={"CHECK( init(main()), LTL(G valid-free) )\n " +
          "CHECK( init(main()), LTL(G valid-deref) )\n " +
          "CHECK( init(main()), LTL(G valid-deref) )\n " +
          "CHECK( init(main()), LTL(G valid-deref) )\n " +
          "CHECK( init(main()), LTL(G valid-memtrack) )"
        }>Memory Safety</MenuItem>
        <MenuItem value={"CHECK( init(main()), LTL(G valid-memcleanup) )"}>Memory Cleanup</MenuItem>
        <MenuItem value={"CHECK( init(main()), LTL(G ! overflow) )"}>No Overflow</MenuItem>
        <MenuItem value={"CHECK( init(main()), LTL(F end) )"}>Termination</MenuItem>
      </Select>
    </FormControl>
  );
}

type SelectProps = {
  storeInput: Dispatch<SetStateAction<string>>;
}

interface ToolType {
  id: string
  label: string
}

const tools: readonly ToolType[] = [
  { id: '2ls', label: '2LS' },
  { id: 'brick', label: 'BRICK' },
  { id: 'cbmc', label: 'CBMC' },
  { id: 'cpachecker', label: 'CPAchecker' },
  { id: 'dartagnan', label: 'Dartagnan' },
  { id: 'deagle', label: 'Deagle' },
  { id: 'ebf', label: 'EBF' },
  { id: 'esbmc-kind', label: 'ESBMC-kind' },
  { id: 'frama-c-sv', label: 'Frama-C-SV' },
  { id: 'goblint', label: 'Goblint' },
  { id: 'korn', label: 'Korn' },
  { id: 'lart', label: 'LART' },
  { id: 'pesco', label: 'PeSCo' },
  { id: 'sesl', label: 'SESL' },
  { id: 'symbiotic', label: 'Symbiotic' },
  { id: 'uautomizer', label: 'UAutomizer' },
  { id: 'ugemcutter', label: 'UGemCutter' },
  { id: 'ukojak', label: 'UKojak' },
  { id: 'utaipan', label: 'UTaipan' },
  { id: 'veriabs', label: 'VeriAbs' },
  { id: 'verifuzz', label: 'VeriFuzz' },
];

export default App;
