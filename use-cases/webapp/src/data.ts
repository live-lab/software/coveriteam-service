// SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

export const verifierC = `
verifier = ActorFactory.create(ProgramVerifier, verifier_path);

print(verifier);

program = ArtifactFactory.create(CProgram, program_path, data_model);
specification = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':program, 'spec':specification};

result = execute(verifier, inputs);
print(result);
`