# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

cat > /etc/systemd/system/coveriteam-service.service <<- EOF
[Unit]
Description=CoVeriTeam Service
After=network.target

[Service]
User=vagrant
WorkingDirectory=/home/vagrant/coveriteam-service/
ExecStart=python3 -m gunicorn --access-logfile - --bind 0.0.0.0:5000 wsgi:app

[Install]
WantedBy=default.target
EOF

systemctl enable coveriteam-service