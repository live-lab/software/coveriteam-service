# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
#!/bin/bash

REMOTE="${1:-https://coveriteam-service.sosy-lab.org/execute}"
echo "POSTing to $REMOTE"
# Add option --insecure if client machine does not support HTTPS.
curl \
    --form "args=<example_request.json" \
    --form cpachecker.yml=@data/cpachecker.yml \
    --form test02.c=@data/test02.c \
    --form unreach-call.prp=@data/unreach-call.prp \
    --form verifier.cvt=@data/verifier.cvt \
    --output cvt_remote_output.zip \
    $REMOTE
