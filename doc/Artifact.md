<!--
SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

This reproduction package is a virtual machine (VM) to host CoVeriTeam Service described in article <em>CoVeriTeam Service: Verification as a Service</em>.

<em>Abstract.</em>
The research community has developed numerous
tools for solving verification problems, but we are missing a
common interface for executing them. This means users have to
spend considerable effort on the installation and parameter setup,
for each new tool (version) they want to execute. The situation
could make a verification researcher wanting to experiment with
a new verification tool turn away from it.
We aim to make it easier for users to execute verification
tools, as well as provide mechanism for tool developers to make
their tools easily accessible. Our solution combines a web service
and a common interface for verification tools.

<p><em>Contents of this archive</em><p>

```
  |-- LICENSE.txt : specifies the license
  |-- README.txt : this file
  |-- coveriteam-service/ : folder containing CoVeriTeam Service. It has been created from the repository: https://gitlab.com/sosy-lab/software/coveriteam-service
  |-- coveriteam-service-vm.ova: A virtual machine with CoVeriTeam-Service installed and running
```

<p><em>CoVeriTeam Service Virtual Machine</em><p> 

The provided Artifact `coveriteam-service-vm.ova` is a virtual machine with the service pre-installed.
The virtual machine was created with vagrant; the username and password are <em>vagrant</em>.
We recommend to run the VM in *headless* mode: *You do not need to log into the VM, everything can be done from your host machines command line once the vm is running*.
The VM runs Ubuntu22.04 with 2 cores and 5 Gb of memory.
CoVeriTeam Service is included as the systemd service `coveriteam-service`.

To run the artifact, import it with VirtualBox and launch it. The VM is configured to forward CoVeriTeam Service
to `127.0.0.1:5000` on the host machine.

<p><em>Trying CoVeriTeam Service</em><p>

You can open the overview page in your browser under http://127.0.0.1:5000.
To invoke the Service you need to make a POST request to http://127.0.0.1:5000/execute.

*Running an example:*

- Method 1) logging into the VM: Log into the VM - username and password are *vagrant*
```
cd coveriteam-service/doc
./send_request.sh http://127.0.0.1:5000/execute
sudo apt install unzip
unzip cvt_remote_output.zip -d cvt-output
cat cvt-ouput/LOG
```

- Method 2) from your own machine; The commands assumed to start at the root of the unpacked provided zip archive
```
cd coveriteam-service/doc
./send_request.sh http://127.0.0.1:5000/execute
unzip cvt_remote_output.zip -d cvt-output
cat cvt-ouput/LOG
```

<p><em>Execution using cURL</em><p>

This section provides details about what is happening in the `send_request.sh` script for interested readers.

You can use cURL or any other REST client to call the service. The service expects a `multipart/form-data` encoded
POST request with the form field `args` containing a json string containing the inputs to CoVeriTeam and all the files
with the names matching how they are referenced in `args`. 

All of the data for the following example can be found in `coveriteam-service/doc`.

**Example:**

`example_request.json`:
```
{
    "coveriteam_inputs": {
        "verifier_path": "../actors/cpachecker.yml",
        "program_path": "../../sv-benchmarks/c/ldv-regression/test02.c",
        "specification_path": "../../sv-benchmarks/c/properties/unreach-call.prp",
        "verifier_version": "default",
        "data_model": "ILP32"
    },
    "cvt_program": "verifier.cvt",
    "working_directory": "coveriteam/examples",
}
```
```
curl \
    --form "args=<example_request.json" \
    --form "../actors/cpachecker.yml"=@data/cpachecker.yml \
    --form "../../sv-benchmarks/c/ldv-regression/test02.c"=@data/test02.c \
    --form "../../sv-benchmarks/c/properties/unreach-call.prp"=@data/unreach-call.prp \
    --form "verifier.cvt"=@data/verifier.cvt \
    --form "../actors/verifier_resource.yml"=@data/verifier_resource.yml \
    --output cvt_remote_output.zip \
    127.0.0.1:5000/execute
```
