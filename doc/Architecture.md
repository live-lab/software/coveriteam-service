<!--
This file is part of CoVeriTeam Remote:
https://gitlab.com/sosy-lab/software/coveriteam-remote-server

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->
# Software Architecture

```plantuml
@startuml
AbstractRequestProcessor <|-- RequestProcessorVCloud
InputValidatorBase <|-- InputValidatorVCloud
InputDataContract "1 \ninput" <-- InputValidatorBase
InputDataContract "1 \ninput" <-- AbstractInputSanitizer
InputDataContract "1 \ninput" <-- AbstractRequestProcessor
ActorConfig <.. RequestProcessorVCloud
VcloudExecutorUtil <.. RequestProcessorVCloud

AbstractInputSanitizer <|-- InputSanitizerVCloud

class AbstractRequestProcessor {
#_tmp: str
#_wd: Path
+cleanup(self)
+join_wd(self,p)
+process_request(self)
{abstract}_prechecks(self)
{abstract}_create_required_files(self)
{abstract}_execute_coveriteam(self)
{abstract}_prepare_response(self)
}

class AbstractInputSanitizer {
+sanitize_input(self)
{abstract}sanitize_working_directory(self)
{abstract}sanitize_cvt_program(self)
{abstract}sanitize_coveriteam_inputs(self)
{abstract}sanitize_files(self)
}

class InputDataContract {
+working_directory
+coveriteam_inputs
+cvt_program
+files
+type_check_input_data_contract(self)
{static}check_string(s:str)
}

class RequestProcessorVCloud {
#_prechecks(self)
#_create_required_files(self)
#_execute_coveriteam(self)
-__download_tools(self): (required_tools: list, required_archives: list)
#_prepare_response(self)
-__create_benchdef_xml(self, required_tools: list, required_archives: list)
-__save_benchdef_xml(self, xml_elem)
}

class InputSanitizerVCloud {
#sanitize_working_directory(self)
#sanitize_cvt_program(self)
#sanitize_coveriteam_inputs(self)
#sanitize_files(self)
}

class InputValidatorBase {
-__init__(self, input_data_contract)
+validate_input(self)
-__validate_working_directory(self)
-__validate_cvt_program(self)
-__validate_coveriteam_inputs(self)
-__validate_files(self)
#check_allowed_file_path(self, path)
#custom_validation(self)
}

class InputValidatorVCloud {
+ALLOWED_INPUT_KEYS
#custom_validation(self)
#check_allowed_file_path(self, fp)
}

class ActorConfig {
-__ALLOWED_URL_PREFIXES_FOR_ARCHIVES
+actor_name
+archive_location
+tool_info
-{static}__load_actor_def(yaml_file)
-__install_if_needed(self)
-__resolve_tool_info_module(self)
-{static}__is_url(path_or_url)
-{static}__download_if_needed(url, target)
-{static}__unzip(archive, target_dir)
}

class VcloudExecutorUtil {
+RESULT_XML_PUBLIC_ID
+RESULT_XML_SYSTEM_ID
-_COVERITEAM_REPO
-_BENCHMARKS_REPO
+BENCHMARK_ELEM_DICT
+REQUIRE_ELEM_DICT
-_BENCHMARK_PY
+BENCHDEF_XML
+BENCHEXEC_OUTPUT_PATH

+{static}get_command(tmpdir)
-{static}__find_stem_for_benchmarks_repo(path, folder_name)
+{static}guess_file_url(path)
+{static}save_file_from_url(url, target)
}

@enduml
```

